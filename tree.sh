`- modern   # gospel, praise, etc
	`- scores
		`- pdf
		`- mscore
		`- other
	`- chords
		`- chordpro
		`- pdf
		`- other
	`- lyrics
		`- pdf
		`- office  # .odt, .doc, .docx
		`- txt
`- classical  # gregorian, etc
	`- scores
		`- pdf
		`- mscore
		`- other
	`- chords
		`- chordpro
		`- pdf
		`- other
	`- lyrics
		`- pdf
		`- office  # .odt, .doc, .docx
		`- txt
`- jks  # something between classical and modern; let's say c 20th century
	`- scores
		`- pdf
		`- mscore
		`- other
	`- chords
		`- chordpro
		`- pdf
		`- other
	`- lyrics
		`- pdf
		`- office  # .odt, .doc, .docx
		`- txt
	`- presentations  # .odp, .ppt, .pptx
`- devotions
	`- scores
		`- pdf
		`- mscore
		`- other
	`- chords
		`- chordpro
		`- pdf
		`- other
	`- lyrics
		`- pdf
		`- office  # .odt, .doc, .docx
		`- txt
	`- presentations  # .odp, .ppt, .pptx
`- songbooks  # some selected songbooks (e.g. Spievatko, Taize)
	`- scores
		`- pdf
		`- mscore
		`- other
	`- chords
		`- chordpro
		`- pdf
		`- other
	`- lyrics
		`- pdf
		`- office  # .odt, .doc, .docx
		`- txt